create table tb_article(
    id int primary key auto_increment,
    category_id int not null,
    title varchar (50),
    description varchar (255),
    author varchar (50),
    thumbnail varchar (100),
    create_date varchar (50)
);
create table tb_category(
    id int primary key auto_increment,
    category varchar (50),
    category_description varchar(255)
);
