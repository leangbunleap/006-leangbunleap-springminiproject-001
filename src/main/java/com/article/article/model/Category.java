package com.article.article.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Category {
    @NotNull
    private int id;
    @NotBlank
    private String category;
    @NotBlank
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category(int id, String category, String description) {
        this.id = id;
        this.category = category;
        this.description = description;
    }

    public Category(){}

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
