package com.article.article.controller;

import com.article.article.model.Article;
import com.article.article.model.Category;
import com.article.article.service.serviceImp.ArticleServiceImp;
import com.article.article.service.serviceImp.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;


@Controller
public class ArticleController {
    private ArticleServiceImp articleServiceImp;
    private CategoryServiceImp categoryServiceImp;

    @Autowired
    public void setCategoryServiceImp(CategoryServiceImp categoryServiceImp) {
        this.categoryServiceImp = categoryServiceImp;
    }
    @Autowired
    public void setArticleServiceImp(ArticleServiceImp articleServiceImp) {
        this.articleServiceImp = articleServiceImp;
    }

    @GetMapping("/update")
    public String update(@RequestParam("id")Integer id,ModelMap model){
        Article article=articleServiceImp.getArticleById(id);
        model.addAttribute("article",article);
        ArrayList<Category> categories=categoryServiceImp.getCategory();
        if(categories.size()>0){
            model.addAttribute("categories",categories);
        }
        return "edit";
    }
    @PostMapping("/update")
    public String updateData(@Valid @ModelAttribute Article article, BindingResult result,Model model,@RequestParam("file") MultipartFile file){
        if(result.hasErrors()){
            model.addAttribute("article", article);
            return "edit";
        }
        if (!file.isEmpty()){
            String nameFile= UUID.randomUUID().toString()+file.getOriginalFilename();
            try {
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"/src/main/resources/images/",nameFile));
                article.setThumbnail("/images/"+nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            article.setThumbnail(article.getThumbnail());
        }

        articleServiceImp.updateAricleById(article,article.getId());
        return "redirect:/";
    }

    @GetMapping("/add")
    public String add(ModelMap m){
        ArrayList<Category> categories=categoryServiceImp.getCategory();
        if(categories.size()>0){
            m.addAttribute("categories",categories);
        }
        m.addAttribute("article",new Article());
        return "add";
    }
    @PostMapping("/add")
    public String addArticle(@Valid @ModelAttribute Article article,BindingResult result,Model model,@RequestParam("file") MultipartFile file){

        if(result.hasErrors()){
            model.addAttribute("article",article);
            return "add";
        }
        if (!file.isEmpty()){
            System.out.println(file.getOriginalFilename());
            String nameFile= UUID.randomUUID().toString()+file.getOriginalFilename();

            try {
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"/src/main/resources/images/",nameFile));
                article.setThumbnail("/images/"+nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        articleServiceImp.addArticle(article);
        return "redirect:/";
    }

    @GetMapping("/delete")
    public String deleteArticle(@RequestParam("id") Integer id,
                                @RequestParam("category_id") int category_id){
        articleServiceImp.deleteArticle(id);
        return "redirect:/?category_id=" + category_id;
    }

    @GetMapping("/view/{id}")
    public String viewArticle(@PathVariable Integer id,Model model){
        Article article=articleServiceImp.getArticleById(id);
        model.addAttribute("article",article);
        return "view";
    }

    @GetMapping("/")
    public String serachArticle(@RequestParam(value = "title",defaultValue = "") String title,
                                @RequestParam(value = "page" , defaultValue = "1") Integer page,
                                @RequestParam(value = "limit" , defaultValue = "5") int limit,
                                @RequestParam(value = "category_id",defaultValue = "0")int category_id,Model model){
        ArrayList<Article> articles=articleServiceImp.searchArticleByTitle(title,page,limit,category_id);
        int records = articleServiceImp.countRecordsByTitle(title,category_id);
        int pages = (int) Math.ceil((double) records/limit);
        ArrayList<Integer> indexs = new ArrayList<>();

        for(int i=1;i<=pages;i++){
            indexs.add(i);
        }

        model.addAttribute("category_id",category_id);
        model.addAttribute("title",title);
        model.addAttribute("limit",limit);
        model.addAttribute("numberIndex",indexs);
        model.addAttribute("page",page);
        ArrayList<Category> categories=categoryServiceImp.getCategory();
        if(categories.size()>0){
            model.addAttribute("categories",categories);
        }
        model.addAttribute("articles",articles);
        return "index";
    }
}
