package com.article.article.controller;

import com.article.article.model.Article;
import com.article.article.model.Category;
import com.article.article.service.serviceImp.ArticleServiceImp;
import com.article.article.service.serviceImp.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;


@Controller
public class CategoryController {

    private CategoryServiceImp categoryServiceImp;
    @Autowired
    public void setCategoryServiceImp(CategoryServiceImp categoryServiceImp) {
        this.categoryServiceImp = categoryServiceImp;
    }
    @GetMapping("/category")
    public String getCategory(@RequestParam(value = "page",defaultValue = "1")Integer page,
                              @RequestParam(value = "limit",defaultValue = "5")int limit,
                              Model model){
        System.out.println("page="+page);
        System.out.println("limit="+limit);
        int records = categoryServiceImp.countRecord();
        int pages = (int) Math.ceil((double) records/limit);
        ArrayList<Integer> indexs = new ArrayList<>();
        for(int i=1; i <= pages; i++){
            indexs.add(i);
        }
        model.addAttribute("limit",limit);
        model.addAttribute("numberIndex",indexs);
        model.addAttribute("page",page);
        model.addAttribute("categories",categoryServiceImp.getCategoryByPage(page,limit));
        return "category";
    }

    @GetMapping("/update_category/{id}")
    public String updateCategory(@PathVariable Integer id , ModelMap model){
        Category category=categoryServiceImp.getCategoryById(id);
        System.out.println(category.getDescription());
        model.addAttribute("category",category);
        return "edit_category";
    }
    @PostMapping("/update_category")
    public String updateCategoryData(@Valid @ModelAttribute Category category, BindingResult result,Model model){
        if(result.hasErrors()){
            model.addAttribute("category",category);
            return "edit_category";
        }
        categoryServiceImp.updateCategoryById(category,category.getId());
        return "redirect:/category";
    }

    @GetMapping("/add_category")
    public String addCategory(ModelMap m){
        m.addAttribute("category",new Category());
        return "add_category";
    }
    @PostMapping("/add_category")
    public String addCategoryData(@Valid @ModelAttribute Category category,BindingResult result,Model model){
        if(result.hasErrors()){
            model.addAttribute("category",category);
            return "add_category";
        }
        categoryServiceImp.addCategory(category);
        return "redirect:/category";
    }
    @GetMapping("/delete_category/{id}")
    public String deleteCategory(@PathVariable Integer id){
        categoryServiceImp.deleteCategory(id);
        return "redirect:/category";
    }

}
