package com.article.article.service;

import com.article.article.model.Article;
import com.article.article.model.Category;

import java.util.ArrayList;

public interface CategoryService {
    ArrayList<Category> getCategory();
    ArrayList<Category> getCategoryByPage(int page,int limit);
    Category getCategoryById(Integer id);
    void updateCategoryById(Category article, Integer id);
    void addCategory(Category category);
    void deleteCategory(Integer id);
    int countRecord();
}
