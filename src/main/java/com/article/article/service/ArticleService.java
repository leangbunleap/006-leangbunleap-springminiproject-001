package com.article.article.service;

import com.article.article.model.Article;

import java.util.ArrayList;

public interface ArticleService {
   // ArrayList<Article> getArticles();
    Article getArticleById(Integer id);
    void updateAricleById(Article article,Integer id);
    void addArticle(Article article);
    void deleteArticle(Integer id);
    ArrayList<Article> searchArticleByTitle(String title,int page,int limi,int category_id);
    public int countRecordsByTitle(String title,int category_id);
}
