package com.article.article.service.serviceImp;

import com.article.article.model.Article;
import com.article.article.respositeries.ArticleRespositery;
import com.article.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Service
public class ArticleServiceImp implements ArticleService {

    private ArticleRespositery articleRespositery;
    @Autowired
    public void setArticleRespositery(ArticleRespositery articleRespositery){
        this.articleRespositery = articleRespositery;
    }

//    @Override
//    public ArrayList<Article> getArticles() {
//        return articleRespositery.getArticles();
//    }

    @Override
    public Article getArticleById(Integer id) {
        return articleRespositery.getArticleById(id);
    }

    @Override
    public void updateAricleById(Article article,Integer id) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = formatter.format(new Date());
        article.setCreate_date(strDate);
        articleRespositery.updateAricleById(article,id);
    }


    @Override
    public void addArticle(Article article) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = formatter.format(new Date());
        article.setCreate_date(strDate);
        articleRespositery.addAricle(article);
    }

    @Override
    public void deleteArticle(Integer id) {
        articleRespositery.deleteArticle(id);
    }

    @Override
    public ArrayList<Article> searchArticleByTitle(String title,int page,int limit,int category_id) {
        return articleRespositery.searchArticleByTitle(title,page,limit,category_id);
    }

    @Override
    public int countRecordsByTitle(String title,int category_id) {
        return articleRespositery.countRecordsByTitle(title,category_id);
    }
}
