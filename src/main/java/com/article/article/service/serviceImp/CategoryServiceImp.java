package com.article.article.service.serviceImp;

import com.article.article.model.Article;
import com.article.article.model.Category;
import com.article.article.respositeries.ArticleRespositery;
import com.article.article.respositeries.CategoryRespositery;
import com.article.article.service.ArticleService;
import com.article.article.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Service
public class CategoryServiceImp implements CategoryService{

    private CategoryRespositery categoryRespositery;
    @Autowired
    public void setCategoryRespositery(CategoryRespositery categoryRespositery) {
        this.categoryRespositery = categoryRespositery;
    }

    @Override
    public ArrayList<Category> getCategoryByPage(int page,int limit) {
        return categoryRespositery.getCategoryByPage(page,limit);
    }

    @Override
    public Category getCategoryById(Integer id) {
        return categoryRespositery.getCategoryById(id);
    }

    @Override
    public void updateCategoryById(Category category,Integer id) {
        categoryRespositery.updateCategoryById(category,id);
    }

    @Override
    public void addCategory(Category category) {
        categoryRespositery.addCategory(category);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryRespositery.deleteCategory(id);
    }

    @Override
    public int countRecord() {
        return categoryRespositery.countRecord();
    }

    @Override
    public ArrayList<Category> getCategory() {
        return categoryRespositery.getCategory();
    }
}
