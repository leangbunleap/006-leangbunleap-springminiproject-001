package com.article.article.respositeries;

import com.article.article.model.Article;
import com.article.article.model.Category;
import com.article.article.respositeries.providers.ArticleProviders;
import com.article.article.respositeries.providers.CategoryProviders;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface CategoryRespositery {
    @SelectProvider(method = "selectAllCategory",type = CategoryProviders.class)
    ArrayList<Category> getCategory();
    @SelectProvider(method = "selectAll",type = CategoryProviders.class)
    @Results({
            @Result(property = "description",column = "category_description")
    })
    ArrayList<Category> getCategoryByPage(int page,int limit);

    @SelectProvider(method = "selectCount",type = CategoryProviders.class)
    int countRecord();

    @SelectProvider(method = "findCategoryById",type = CategoryProviders.class)
    @Results({
            @Result(property = "description", column = "category_description")
    })
    Category getCategoryById(int id);
    @SelectProvider(method = "updateCategory",type = CategoryProviders.class)
    @Results({
            @Result(property = "description", column = "category_description")
    })
    void updateCategoryById(Category category, Integer id);
    @SelectProvider(method = "addCategory",type = CategoryProviders.class)
    @Results({
            @Result(property = "description", column = "category_description")
    })
    void addCategory(Category category);
    @SelectProvider(method = "deleteCategory",type = CategoryProviders.class)
    @Results({
            @Result(property = "description",column = "category_description")
    })
    void deleteCategory(Integer id);
}
