package com.article.article.respositeries.providers;

import com.article.article.model.Article;
import com.article.article.model.Category;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class CategoryProviders {

    public String findCategoryById(Integer id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String addCategory(Category category){
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("category","#{category}");
            VALUES("category_description","#{description}");
        }}.toString();
    }
    public String deleteCategory(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String updateCategory(Category category,Integer id){
        return new SQL(){{
            UPDATE("tb_category");
            SET("category=#{category.category}");
            SET("category_description=#{category.description}");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String selectAll(int page,int limit){
        int offset = (page - 1) * limit;
        String sql="select * from tb_category"+
                " limit "+limit+" OFFSET " + offset;
        return sql;
    }
    public String selectCount(){
        String sql="select count(*) from tb_category";
        return sql;
    }
    public String selectAllCategory(){
        String sql="select * from tb_category";
        return sql;
    }
}
