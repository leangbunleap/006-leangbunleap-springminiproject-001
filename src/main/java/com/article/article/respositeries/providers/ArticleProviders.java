package com.article.article.respositeries.providers;

import com.article.article.model.Article;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class ArticleProviders {
    public String findArticleById(Integer id){
        String sql = "select a.id," +
                "c.category," +
                "a.title," +
                "a.description," +
                "a.author," +
                "a.thumbnail," +
                "a.create_date" +
                " from tb_article a inner join" +
                " tb_category c on a.category_id = c.id " +
                " where a.id =#{id}";
        return sql;
    }
    public String addArticleProvider(Article article){
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("title","#{title}");
            VALUES("description","#{description}");
            VALUES("author","#{author}");
            VALUES("category_id","#{category_id}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("create_date","#{create_date}");
        }}.toString();
    }
    public String deleteArticle(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String updateArticle(Article article,Integer id){
        return new SQL(){{
            UPDATE("tb_article");
            SET("category_id=#{article.category_id}");
            SET("title=#{article.title}");
            SET("description=#{article.description}");
            SET("author=#{article.author}");
            SET("thumbnail=#{article.thumbnail}");
            SET("create_date=#{article.create_date}");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String searchArticle(String title,int page,int limit,int category_id){
        int offset = (page - 1) * limit;
        String sql1="select a.id," +
                "c.category," +
                "a.title," +
                "a.description," +
                "a.author," +
                "a.thumbnail," +
                "a.create_date" +
                " from tb_article a inner join" +
                " tb_category c on a.category_id = c.id " +
                " where a.title ILIKE '%'||#{title}||'%' and a.category_id= "+category_id+""+
                " limit "+limit+" OFFSET " + offset;
        String sql2="select a.id," +
                "c.category," +
                "a.title," +
                "a.description," +
                "a.author," +
                "a.thumbnail," +
                "a.create_date" +
                " from tb_article a inner join" +
                " tb_category c on a.category_id = c.id " +
                " where a.title ILIKE '%'||#{title}||'%'"+
                " limit "+limit+" OFFSET " + offset;
        if(category_id>0)
            return sql1;
        return sql2;
    }
    public String countRecordsByTitle(String title,int category_id){
        String sql1="select count(*) "+
                " from tb_article a inner join" +
                " tb_category c on a.category_id = c.id "+
                " where a.title ILIKE '%'||#{title}||'%' and a.category_id= "+category_id+"";
        String sql2="select count(*) "+
                " from tb_article a inner join" +
                " tb_category c on a.category_id = c.id "+
                " where a.title ILIKE '%'||#{title}||'%'";
        if(category_id>0)
            return sql1;
        return sql2;

    }
}
