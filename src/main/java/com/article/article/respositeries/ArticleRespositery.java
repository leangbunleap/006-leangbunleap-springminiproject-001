package com.article.article.respositeries;

import com.article.article.model.Article;
import com.article.article.respositeries.providers.ArticleProviders;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ArticleRespositery {

    @SelectProvider(method = "findArticleById",type = ArticleProviders.class)
    @Results({
        @Result(property = "category",column ="category")
    })
    Article getArticleById(int id);
    @SelectProvider(method = "updateArticle",type = ArticleProviders.class)
    @Results({
           @Result(property = "category_id",column ="category_id")
    })
    void updateAricleById(Article article,Integer id);

    @SelectProvider(method = "addArticleProvider",type = ArticleProviders.class)
    void addAricle(Article article);

    @SelectProvider(method = "deleteArticle",type = ArticleProviders.class)
    void deleteArticle(Integer id);

    @SelectProvider(method = "searchArticle",type = ArticleProviders.class)
    @Results({
            @Result(property = "category",column ="category")
    })
    ArrayList<Article> searchArticleByTitle(String title , @Param("page") int page , @Param("limit") int limit,@Param("category_id") int category_id);
    @SelectProvider(method = "countRecordsByTitle",type = ArticleProviders.class)
    public int countRecordsByTitle(@Param("title") String title,@Param("category_id") int category_id);
}
